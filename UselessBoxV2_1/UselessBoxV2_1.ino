
#include <EEPROM.h>
#include <Servo.h>
#include "FastLED.h"

#define NUM_LEDS 1

#define LED_DATA_PIN 8

#define SOFT_POWER_PIN 7

#define MODE_NORMAL 0
#define MODE_CALIBRATING 1
#define MODE_TESTING -1
#define MODE_SETSERVOS 2

int mode = MODE_NORMAL;

int randomThingDelay = 0;

CRGB leds[NUM_LEDS];

#define X 2
#define Y 3

Servo x; // create servo object to control a servo
Servo y;

int currentXPosition = 90;
int currentYPosition = 90;

int switchPositions[] = {70, 87, 108, 123, 142}; // values are replaced from EEPROM
int switchPins[] = {13, 12, 11, 10, 9};

int yRetract = 0;
int yMax = 140;
int yPeek = 100;

bool motorsAttached = false;

int switchState[] = {0, 0, 0, 0, 0};
int switchStatePrevious[] = {0, 0, 0, 0, 0};
long lastOn[] = {0, 0, 0, 0, 0}; // records the last time each switch was turned on (used to detect special patterns like for TEST mode)
int timesInARow[] = {0, 0, 0, 0, 0};

long lastLEDChange = 0;
CRGB currentLEDColor;

// Calibration Mode variables
long calibrationStartTime = 0;
int calibrationTargetSwitch = -1;
long lastCalibrationStep = 0;
int calibrationControlSwitches[] = {0, 0, 0, 0, 0};

// Normal Mode variables
#define SUBMODE_IDLE 0
#define SUBMODE_MOVINGTOSWITCH 1
#define SUBMODE_REPLACINGTONGUE 2
#define SUBMODE_SWEEPINGTOSWITCH 3
int currentSubMode = SUBMODE_IDLE;
int targetSwitch = -1;
long responseDelay = 0;
long responseStart = 0;

void setup()
{
  pinMode(SOFT_POWER_PIN, OUTPUT);
  pinMode(A0, INPUT);
  digitalWrite(SOFT_POWER_PIN, HIGH);
  Serial.begin(115200);
  FastLED.addLeds<WS2812B, LED_DATA_PIN, GRB>(leds, NUM_LEDS);
  for (int i = 0; i < 5; i++)
  {
    pinMode(switchPins[i], INPUT_PULLUP);
  }
  LoadEEPROMValues();
  SetColor(20, 0, 20);
}

void LoadEEPROMValues()
{
  Serial.print("Loading values from EEPROM: ");
  for (int i = 0; i < 5; i++)
  {
    switchPositions[i] = EEPROM.read(i);
    Serial.print(switchPositions[i]);
    Serial.print(" ");
  }
  yMax = EEPROM.read(5);
  Serial.print(" y: ");
  Serial.println(yMax);
  yRetract = 0;
}

void loop()
{
  UpdateSwitchState();

  switch (mode)
  {
  case MODE_SETSERVOS:
    LoopSetServos();
    break;
  case MODE_NORMAL:
    LoopNormal();
    break;
  case MODE_CALIBRATING:
    LoopCalibrating();
    break;
  case MODE_TESTING:
    LoopTesting();
    break;
  }

  delay(20);
}

void LoopSetServos()
{
  if (millis() - lastLEDChange > 500)
  {
    if (currentLEDColor == CRGB(50, 50, 0))
    {
      SetColor(0, 50, 50);
    }
    else
    {
      SetColor(50, 50, 0);
    }
  }

  ServosOn();

  x.write(140);
  y.write(20);

  if (AllSwitchesOn())
  {
    mode = MODE_NORMAL;
  }
}

void LoopNormal()
{
  //Serial.println(currentSubMode);
  if (currentSubMode == SUBMODE_IDLE)
  {
    int i = random(5);
    if (SwitchOn(i) && millis() - responseStart > 450)
    {

      targetSwitch = i;
      currentSubMode = SUBMODE_SWEEPINGTOSWITCH;
      Serial.println("to SUBMODE_SWEEPINGTOSWITCH");
      ServosOn();
      x.write(switchPositions[i]);
      currentXPosition = switchPositions[i];
      y.write(yPeek);
      currentYPosition = yPeek;
      responseStart = millis();
      responseDelay = difference(switchPositions[i], currentXPosition) * 4;
      Serial.println(responseStart);
      return;
    }

    if (millis() - responseStart > 200)
    {
      x.write(90);
    }
    if (millis() - responseStart > 1000)
    {
      ServosOff();
    }

    if (millis() - responseStart > randomThingDelay)
    {
      DoRandomThing();      
    }
  }
  else if (currentSubMode == SUBMODE_SWEEPINGTOSWITCH)
  {
    Serial.println("SUBMODE_SWEEPINGTOSWITCH");
    if (millis() - responseStart > responseDelay)
    {
      x.write(switchPositions[targetSwitch]);
      currentXPosition = switchPositions[targetSwitch];
      y.write(yMax);
      currentYPosition = yMax;
      currentSubMode = SUBMODE_MOVINGTOSWITCH;
    }
  }
  else if (currentSubMode == SUBMODE_MOVINGTOSWITCH)
  {
    Serial.println("SUBMODE_REPLACINGTONGUE");
    if (!SwitchOn(targetSwitch))
    {
      y.write(yRetract);
      currentYPosition = yRetract;
      currentSubMode = SUBMODE_IDLE;
      responseStart = millis();
    }
  }
}

int difference(int a, int b)
{
  if (a > b)
    return a - b;
  else
    return b - a;
}

void LoopTesting()
{
  x.attach(X);
  Serial.println("looptesting");
  x.write(80);
  delay(1000);
  x.write(120);
  delay(1000);
}
