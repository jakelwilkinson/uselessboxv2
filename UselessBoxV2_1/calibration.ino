void LoopCalibrating()
{
  if (AllSwitchesOn()){
    mode = MODE_NORMAL;
    responseStart = millis();
    return;
  }
  if (millis() - lastLEDChange > 500)
  {
    if (currentLEDColor == CRGB(50, 0, 0))
    {
      SetColor(0, 0, 50);
    }
    else
    {
      SetColor(50, 0, 0);
    }
  }

  // Don't start reading swtiches until calibration mode has been on for 2 seconds
  if (millis() - calibrationStartTime > 2000)
  {

    // If no calibration target selected yet
    if (calibrationTargetSwitch == -1)
    {
      for (int i = 0; i < 5; i++)
      {
        if (SwitchToggledOn(i))
        {
          calibrationTargetSwitch = i;
          Serial.print("calibration target set ");
          Serial.println(calibrationTargetSwitch);
          x.attach(X);
          y.attach(Y);
          x.write(currentXPosition);
          y.write(currentYPosition);
          return;
        }
        else
        {
          x.attach(X);
          y.attach(Y);
        }
      }
    }
    else
    {
      // Control the calibration here
      if (millis() - lastCalibrationStep > 100)
      {
        AssignCalibrationControlSwitches();
        lastCalibrationStep = millis();

        if (CalLeft())
        {
          currentXPosition += 1;
          if (currentXPosition > 180)
          {
            currentXPosition = 180;
          }
          Serial.print("iterating x position ");
          Serial.println(currentXPosition);
        }
        else if (CalRight())
        {
          currentXPosition -= 1;
          if (currentXPosition < 0)
          {
            currentXPosition = 0;
          }
        }
        else if (CalFwd())
        {
          currentYPosition -= 1;
          if (currentYPosition < 0)
          {
            currentYPosition = 0;
          }
        }
        else if (CalBack())
        {
          currentYPosition += 1;
          if (currentYPosition > 180)
          {
            currentYPosition = 180;
          }
        }

        if (AllSwitchesOff())
        {
          EEPROM.write(calibrationTargetSwitch, currentXPosition);
          EEPROM.write(5, currentYPosition);     
          switchPositions[calibrationTargetSwitch] = currentXPosition;     
          calibrationTargetSwitch = -1;
          currentXPosition = 90;
          currentYPosition = 90;
          x.attach(X);
          y.attach(Y);
          x.write(currentXPosition);
          y.write(currentYPosition);
          delay(500);
        }

        x.attach(X);
        y.attach(Y);
        x.write(currentXPosition);
        y.write(currentYPosition);
        Serial.println(currentXPosition);
      }
    }
  }
}


int CalFwd()
{
  return SwitchOn(calibrationControlSwitches[0]) &&
         !SwitchOn(calibrationControlSwitches[1]);
}
int CalBack()
{
  return !SwitchOn(calibrationControlSwitches[0]) &&
         SwitchOn(calibrationControlSwitches[1]);
}
int CalLeft()
{
  return !SwitchOn(calibrationControlSwitches[2]) &&
         SwitchOn(calibrationControlSwitches[3]);
}
int CalRight()
{
  return SwitchOn(calibrationControlSwitches[2]) &&
         !SwitchOn(calibrationControlSwitches[3]);
}

void AssignCalibrationControlSwitches()
{
  int counter = 0;
  for (int sw = 0; sw < 5; sw++)
  {
    if (calibrationTargetSwitch != sw)
    {
      calibrationControlSwitches[counter] = sw;
      counter++;
    }
  }
}