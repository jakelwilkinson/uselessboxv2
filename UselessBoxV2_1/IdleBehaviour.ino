

void DoRandomThing()
{
  Serial.println("Doing Random Thing");

  ShutDown();
  
}




void ShutDown()
{
  Serial.println("Shutting Down");
  digitalWrite(SOFT_POWER_PIN, LOW);
  delay(10000);
}





/* IMPORTANT COMMANDS 


ServosOn()       This turns on the servos, in normal mode they are turned off after every move to save battery
ServosOff()      Turns the servos off to save battery, if the motors aren't doing anything for a while, consider using this.

x.write(90)      Turns the X servo to the 90degree position

random(50)       Returns a random number from 0 to 49

for (int i = 0; i < 150; i = i + 1){         A loop that counts i from 0 to 150 and writes it to the servo
  x.write(i)
  delay(20)                                  this delay makes the loop take longer
}

for (int i = 150; i > 0; i = i - 1){         A loop that counts DOWN i from 150 to 0 and writes it to the servo
  x.write(i)
  delay(20)
}



*/


























