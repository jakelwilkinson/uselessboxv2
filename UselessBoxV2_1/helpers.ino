
void SetColor(int r, int g, int b)
{
  leds[0] = CRGB(r, g, b);
  FastLED.show();
  lastLEDChange = millis();
  currentLEDColor = CRGB(r, g, b);
}

bool SwitchToggledOn(int i)
{
  return (switchState[i] && !switchStatePrevious[i]);
}

bool SwitchToggledOff(int i)
{
  return (!switchState[i] && switchStatePrevious[i]);
}

bool SwitchOn(int i)
{
  return switchState[i];
}

bool AllSwitchesOff()
{
  bool isOff = true;
  for (int i = 0; i < 5; i++)
  {
    if (SwitchOn(i))
    {
      isOff = false;
    }
  }
  return isOff;
}

bool AllSwitchesOn()
{
  bool isOn = true;
  for (int i = 0; i < 5; i++)
  {
    if (!SwitchOn(i))
    {
      isOn = false;
    }
  }
  return isOn;
}

void UpdateSwitchState()
{
  for (int i = 0; i < 5; i++)
  {
    switchStatePrevious[i] = switchState[i];
    switchState[i] = digitalRead(switchPins[i]);

    if (switchState[i] && !switchStatePrevious[i])
    {
      Serial.print(switchState[i]);
      Serial.print(switchStatePrevious[i]);
      if (millis() - lastOn[i] < 300)
      {
        timesInARow[i]++;
        Serial.print(" switch ");
        Serial.print(i);
        Serial.print(" toggled ");
        Serial.print(timesInARow[i]);
        Serial.println(" times in a row");

        if (i == 4 && timesInARow[i] == 10)
        {
          mode = MODE_CALIBRATING;
          calibrationStartTime = millis();
        }
        else if (i == 0 && timesInARow[i] == 10)
        {
          mode = MODE_SETSERVOS;
        }
      }
      else
      {
        timesInARow[i] = 0;
        Serial.print(" switch ");
        Serial.print(i);
        Serial.println(" reset");
      }
      lastOn[i] = millis();
    }
  }
}

void ServosOn()
{
  x.attach(X);
  y.attach(Y);
}

void ServosOff()
{
  x.detach();
  y.detach();
}